# OpenBikeHero

An application for finding [Bicycle Repair Stands](https://wiki.openstreetmap.org/wiki/Tag:amenity%3Dbicycle_repair_station).

In early stages of development. Powered by OSM and replacing bikehero.io (currently broken)

The map style is [OpenTrailStash](https://gitlab.com/trailstash/openstyle) with some run-time
modifications and the Overpass layer for repair stands is powered by [Overpass
Ultra](https://gitlab.com/trailstash/overpass-ultra)'s live tiler.
