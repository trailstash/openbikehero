import maplibregl from "https://cdn.skypack.dev/pin/maplibre-gl@v2.4.0-H4IHKk1NcQVlmkNN8f4I/mode=imports,min/optimized/maplibre-gl.js";
import { MapGlLiveTile } from "https://overpass-ultra.trailsta.sh/livetile.js";
import { osmAuth } from "https://cdn.skypack.dev/pin/@dschep/osm-auth@v2.1.0-PrpVsI0qhaGfJOPe2yqh/mode=imports,min/optimized/@dschep/osm-auth.js";
import { h, t } from "./dom.js";

const userAdded = "user-added";
const fromOverpass = "OverpassAPI";

var auth = osmAuth({
  client_id: "iiuelKo8sjchAlJCxEV483KsOIbm0YMvVaemxLz_F4Y",
  client_secret: "2_C5gxwOsk3t11oZFpbsS0y8Eqohjv3PahuORheLLus",
  redirect_uri: window.location.origin + window.location.pathname,
  singlepage: true,
  scope: "write_api",
  url: "https://master.apis.dev.openstreetmap.org",
});
if (window.location.hostname === "bikehero.io") {
  auth = osmAuth({
    client_id: "H8u6NMNg87Vv6C4teyTPNVMwQu5aHPFMLI-O4KcFpQs",
    client_secret: "2rT1w3KBxUFmp2izELudfuZnIZM5zKzy81_OtL1iTTE",
    redirect_uri: window.location.origin + window.location.pathname,
    singlepage: true,
    scope: "write_api",
  });
}

var addRepairStand = async (lat, lon) => {
  var changesetXML = `
    <osm>
      <changeset version="0.6" generator="OpenBikeHero">
        <tag k="comment" v="Add bicycle repair station"/>
        <tag k="created_by" v="OpenBikeHero"/>
        <tag k="host" v="${window.location.origin + window.location.pathname}"/>
        <tag k="hashtags" v="#bikehero"/>
      </changeset>
    </osm>
    `;
  const changeset = await auth.fetch("/api/0.6/changeset/create", {
    method: "PUT",
    body: changesetXML,
  });
  var nodeXML = `
    <osm>
      <node id="-1" lon="${lon}" lat="${lat}" version="0" changeset="${changeset}">
        <tag k="amenity" v="bicycle_repair_station"/>
      </node>
    </osm>
    `;
  await auth.fetch("/api/0.6/node/create", {
    method: "PUT",
    body: nodeXML,
  });
  await auth.fetch(`/api/0.6/changeset/${changeset}/close`, {
    method: "PUT",
  });
};

class FALinkControl {
  constructor(link, description, icon) {
    this.link = link;
    this.description = description;
    this.icon = icon;
  }
  onAdd(map) {
    this._map = map;
    this._container = document.createElement("div");
    this._container.className = `mapboxgl-ctrl mapboxgl-ctrl-group ${this.icon}`;
    this._container.addEventListener("contextmenu", (e) => e.preventDefault());
    this._container.addEventListener("click", (e) => this.onClick(e));

    this._container.innerHTML = `
      <div class="tools-box">
        <button>
          <a href="${this.link}" target="_blank" rel="noopener">
            <span class="mapboxgl-ctrl-icon" aria-hidden="true" title="${this.description}">
              <i class="fa fa-lg fa-${this.icon}"></i>
            </span>
          </a>
        </button>
      </div>
    `;

    return this._container;
  }
  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}

function getView() {
  var hash = localStorage.getItem("hash");
  var x = 0;
  var y = 0;
  var z = 0;
  if (hash) {
    [z, y, x] = hash.slice(1).split("/");
  }
  return [z, y, x];
}

function init() {
  var [z, y, x] = getView();
  if (!window.location.hash && x == 0 && y == 0 && z == 0) {
    fetch("https://ipinfo.io/json?token=a6134fb16df81b")
      .then((r) => r.json())
      .then(({ loc }) => {
        center = loc.split(",");
        center = center.reverse();
        map.flyTo({ center, zoom: 10 });
      });
  }
  const userAddedData = { type: "FeatureCollection", features: [] };

  var map = new maplibregl.Map({
    container: "map",
    style: `./style-${
      window.matchMedia &&
      window.matchMedia("(prefers-color-scheme: dark)").matches
        ? "dark"
        : "light"
    }.json`,
    zoom: z,
    center: [x, y],
    hash: true,
  });
  // Livetile Overpass repair station data!
  const livetile = new MapGlLiveTile(
    `[out:json];node[amenity=bicycle_repair_station]({{bbox}});out geom;`,
    10
  );

  map.on("style.load", function () {
    // Add sources
    map.addSource(fromOverpass, {
      type: "geojson",
      data: { type: "FeatureCollection", features: [] },
    });
    map.addSource(userAdded, {
      type: "geojson",
      data: userAddedData,
    });

    // Add icon and then add layers
    map.loadImage("./icon.png", function (error, image) {
      if (error) throw error;
      map.addImage("bicycle_repair_station", image);
      map.addLayer({
        id: "loading-fill",
        type: "fill",
        source: fromOverpass,
        filter: ["==", ["geometry-type"], "Polygon"],
        minzoom: 0,
        paint: {
          "fill-color": "yellow",
          "fill-opacity": 0.25,
        },
      });
      map.addLayer({
        id: "loading-stroke",
        type: "line",
        source: fromOverpass,
        filter: ["==", ["geometry-type"], "Polygon"],
        minzoom: 0,
        paint: {
          "line-color": "blue",
          "line-opacity": 0.5,
          "line-width": 2,
        },
      });
      map.addLayer({
        id: "loading-text",
        type: "symbol",
        source: fromOverpass,
        filter: ["==", ["geometry-type"], "Polygon"],
        minzoom: 0,
        layout: {
          "text-font": ["Noto Sans Regular"],
          "text-field": "Loading...",
          "text-size": 24,
          "text-letter-spacing": 0.3,
        },
      });
      map.addLayer({
        id: fromOverpass,
        type: "symbol",
        source: fromOverpass,
        filter: ["==", ["geometry-type"], "Point"],
        minzoom: 0,
        layout: {
          "icon-image": "bicycle_repair_station",
          "icon-size": 0.5,
        },
      });
      map.addLayer({
        id: userAdded,
        type: "symbol",
        source: userAdded,
        minzoom: 0,
        layout: {
          "icon-image": "bicycle_repair_station",
          "icon-size": 0.5,
        },
      });
    });

    livetile.addTo(map);
  });

  // add controls
  const scale = new maplibregl.ScaleControl();
  map.addControl(scale);
  scale.setUnit("imperial");
  map.addControl(new maplibregl.NavigationControl());
  map.addControl(
    new maplibregl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true,
      },
      trackUserLocation: true,
    })
  );
  var addRepairStandControl = new FALinkControl(
    "#",
    "Add bicycle repair station",
    "plus"
  );
  addRepairStandControl.onClick = (e) => {
    e.preventDefault();
    if (!auth.authenticated()) {
      auth.authenticate();
    } else {
      map.addingBicycleRepairStand = !map.addingBicycleRepairStand;
      updateAddState();
    }
    return false;
  };
  map.addControl(addRepairStandControl);

  var updateAddState = () => {
    if (map.addingBicycleRepairStand) {
      map._container.classList.add("addingBicycleRepairStand");
    } else {
      map._container.classList.remove("addingBicycleRepairStand");
    }
  };
  map.addControl(
    new FALinkControl(
      "https://gitlab.com/trailstash/openbikehero#openbikehero",
      "About",
      "info"
    )
  );

  // add hook to persist view
  map.on("moveend", () =>
    localStorage.setItem(
      "hash",
      `#${map.getZoom(0).toFixed(6)}/${map.getCenter().lat.toFixed(6)}/${map
        .getCenter()
        .lng.toFixed(6)}`
    )
  );

  // handle map click for addition
  map.on("click", (event) => {
    if (!map.addingBicycleRepairStand) {
      return;
    }
    const popup = new maplibregl.Popup({
      closeButton: false,
      closeOnClick: false,
    }).setLngLat(event.lngLat);
    popup
      .setDOMContent(
        h(
          "div",
          {},
          h(
            "button",
            {
              onclick: async () => {
                document.getElementById("uploading").style.display = "block";
                await addRepairStand(event.lngLat.lat, event.lngLat.lng);
                document.getElementById("uploading").style.display = "none";
                map.addingBicycleRepairStand = false;
                updateAddState();
                popup.remove();
                var source = map.getSource("user-added");
                userAddedData.features.push({
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [event.lngLat.lng, event.lngLat.lat],
                  },
                  properties: { amenity: "bicycle_repair_station" },
                });
                source.setData(userAddedData);
              },
            },
            t("Upload")
          ),
          t(" "),
          h(
            "button",
            {
              onclick: () => {
                updateAddState();
                popup.remove();
              },
            },
            t("Cancel")
          )
        )
      )
      .addTo(map);
    return;
  });

  // register service worker for PWA
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("./sw.js").then(function (registration) {
      registration.update();
      console.log("Service Worker Registered");
    });
  }

  if (window.location.search.includes("code")) {
    auth.authenticate(function () {
      history.pushState({}, null, window.location.pathname);
    });
  }
}

init();
