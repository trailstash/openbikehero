SHELL := /bin/bash
.DEFAULT_GOAL = help

# also creates cert.pem
key.pem:
	openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 365 -nodes -subj '/CN=localhost'

serve: key.pem # Start the dev server
	npx http-server --cors --port 8000 -S -c-1

public: index.html index.js dom.js icon.png logo.svg logo.png manifest.json sw.js fontawesome style-light.json style-dark.json # Build gitlab pages public folder
	mkdir -p public
	cp -r $^ $@/.

format: # format with prettier
	npx prettier --write index.*

clean: # Clean up GitLab Pages build & ssl cert
	rm -fr public key.pem cert.pem

.PHONY: serve clean help
help: # Print help
	@awk 'BEGIN {FS = ":.*?# *"} /^[.a-zA-Z_-]+:.*?# */ {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
